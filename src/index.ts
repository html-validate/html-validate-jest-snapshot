import { compatibilityCheck } from "html-validate";
import { name, peerDependencies } from "../package.json";
import { transform } from "./transform";

export default transform;

const range = peerDependencies["html-validate"];
compatibilityCheck(name, range);

import { transformFile } from "html-validate/test-utils";
import { transform } from "./transform";

it("should extract template from snapshot file", async () => {
	expect.assertions(2);
	const result = await transformFile(transform, "./test/__snapshots__/valid.spec.ts.snap");
	expect(result).toHaveLength(1);
	expect(result[0]).toMatchInlineSnapshot(`
		{
		  "column": 30,
		  "data": "
		<body>
		  <main>
		    <h1>
		      lorem ipsum
		    </h1>
		    <p>
		      dolor sit amet
		    </p>
		  </main>
		</body>
		",
		  "filename": "./test/__snapshots__/valid.spec.ts.snap",
		  "line": 3,
		  "offset": 72,
		  "originalData": undefined,
		}
	`);
});

it("should throw error when file is not a valid v1 snapshot file", async () => {
	expect.assertions(3);
	await expect(() =>
		transformFile(transform, "./test/__snapshots__/not-a-snap1.js"),
	).rejects.toThrow("Failed to determine snapshot version");
	await expect(() =>
		transformFile(transform, "./test/__snapshots__/not-a-snap2.js"),
	).rejects.toThrow("Failed to determine snapshot version");
	await expect(() => transformFile(transform, "./test/__snapshots__/v2.js")).rejects.toThrow(
		"Only snapshot v1 is supported but this file is v2",
	);
});

import { type Source } from "html-validate";
import type * as ESTree from "estree";
import * as espree from "espree";
import type * as acorn from "acorn";
import * as walk from "acorn-walk";

declare module "acorn" {
	interface Comment {
		value: string;
	}

	interface Program {
		comments?: Comment[];
	}
}

/* eslint-disable-next-line sonarjs/function-return-type -- works as intended */
function isSnap(program: acorn.Program): string | false {
	const { comments = [] } = program;

	/* fail if snapshot marker cannot be found */
	if (comments.length === 0 || comments[0].start !== 0) {
		return false;
	}

	/* validate version (only v1 is supported) */
	const version = /Jest Snapshot v(\d+),/.exec(comments[0].value);
	if (!version) {
		return false;
	}

	return version[1];
}

function computeOffset(text: string, line: number, column: number): number {
	let offset = column;
	line--; /* dont try to find newline on last line, column is used instead */
	while (line > 0) {
		const p = text.indexOf("\n");
		if (p >= 0) {
			line--;
			text = text.slice(p + 1);
			offset += p;
		} else {
			/* should not happen unless line is actually greater than present in text */
			/* istanbul ignore next: normally not possible to reproduce */
			return offset;
		}
	}
	return offset;
}

/**
 * Extract snapshots from .snap file. It makes *heavy* assumptions but since the
 * format is fixed and a version-check is in place it should be good enough?
 */
function transformSnap(source: Source, ast: acorn.Program): Source[] {
	const sources: Source[] = [];

	/* Walks the AST the following way:
	 *
	 * 1. Locate assignment expressions: exports[`my test case 1`] = `<body/>`
	 *                                   ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	 * 2. Take the rhs and walk the new subtree to locate the template expression: `<body/>`
	 *                                                                             ^^^^^^^^^
	 * 3. Given the template expression the HTML markup is present in the node value.
	 */
	walk.simple(ast as acorn.Node, {
		AssignmentExpression(acornNode) {
			const node = acornNode as ESTree.AssignmentExpression;
			walk.simple(node.right as acorn.Node, {
				TemplateElement(acornNode) {
					const node = acornNode as ESTree.TemplateElement;
					/* eslint-disable-next-line @typescript-eslint/no-non-null-assertion -- `loc` property will always be present when `estree.parse(.., { loc: true }` is set */
					const { line, column } = node.loc!.start;
					sources.push({
						filename: source.filename,
						offset: computeOffset(source.data, line, column),
						data: node.value.raw,
						line,
						column,
						originalData: source.originalData,
					});
				},
			});
		},
	});

	return sources;
}

export function transform(source: Source): Source[] {
	const ast = espree.parse(source.data, {
		ecmaVersion: 2017,
		sourceType: "module",
		comment: true,
		loc: true,
	});

	const version = isSnap(ast);
	if (!version) {
		throw new Error("Failed to determine snapshot version");
	} else if (version !== "1") {
		throw new Error(`Only snapshot v1 is supported but this file is v${version}`);
	}

	return transformSnap(source, ast);
}

transform.api = 1;

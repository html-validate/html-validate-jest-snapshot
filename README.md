# html-validate-jest-snapshot

Transform jest snapshots for use with [html-validate].

Limitations:

- Currently only supports external `.snap` files, i.e. no inline snapshots.
- Cannot differantiate between HTML and other snapshots datatypes. Do no use if
  you use a snapshot serializer like `ConvertAnsi` as it will cause false
  positives.

[html-validate]: https://www.npmjs.com/package/html-validate

## Usage

    npm install --save-dev html-validate-jest-snapshot

In `.htmlvalidate.json`:

```js
{
  "transform": {
    "^.*\\.snap$": "html-validate-jest-snapshot"
  }
}
```

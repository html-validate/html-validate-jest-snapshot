import { FileSystemConfigLoader, HtmlValidate, Message, Report, cjsResolver } from "html-validate";
import transform from "../src";

jest.mock("html-validate-jest-snapshot", () => transform, { virtual: true });

const config = {
	root: true,
	extends: ["html-validate:recommended"],

	transform: {
		"\\.(snap)$": "html-validate-jest-snapshot",
	},
};
const loader = new FileSystemConfigLoader([cjsResolver()], config);

/**
 * Filter out properties not present in all supported versions of html-validate (see
 * peerDependencies). This required in the version matrix integration test.
 */
function filterReport(report: Report): void {
	for (const result of report.results) {
		for (const msg of result.messages) {
			const dst: Partial<Message & { ruleUrl: string }> = msg;
			delete dst.context;
			delete dst.ruleUrl;
			delete dst.selector;
		}
	}
}

it('should find no errors in "valid.spec.ts.snap"', async () => {
	expect.assertions(2);
	const htmlvalidate = new HtmlValidate(loader);
	const report = await htmlvalidate.validateFile("test/__snapshots__/valid.spec.ts.snap");
	filterReport(report);
	expect(report.valid).toBeTruthy();
	expect(report.results).toMatchSnapshot();
});

it('should find errors in "invalid.spec.ts.snap"', async () => {
	expect.assertions(2);
	const htmlvalidate = new HtmlValidate(loader);
	const report = await htmlvalidate.validateFile("test/__snapshots__/invalid.spec.ts.snap");
	filterReport(report);
	expect(report.valid).toBeFalsy();
	expect(report.results).toMatchSnapshot();
});

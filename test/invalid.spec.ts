it("invalid snapshot", () => {
	expect.assertions(1);
	document.body.innerHTML = `<main><h1></h1><p class="foo foo">dolor sit amet</span></div>`;
	expect(document.body).toMatchSnapshot();
});

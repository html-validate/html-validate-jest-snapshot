it("valid snapshot", () => {
	expect.assertions(1);
	document.body.innerHTML = `<main><h1>lorem ipsum</h1><p>dolor sit amet</p></div>`;
	expect(document.body).toMatchSnapshot();
});

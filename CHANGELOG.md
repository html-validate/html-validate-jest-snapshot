# html-validate-jest-snapshot changelog

## 6.0.0 (2024-12-29)

### ⚠ BREAKING CHANGES

- **deps:** require nodejs v18 or later
- **deps:** require html-validate v8 or later

### Features

- **deps:** require html-validate v8 or later ([9a237a2](https://gitlab.com/html-validate/html-validate-jest-snapshot/commit/9a237a20423b06b6fb9a5276af8cd1debadfd1e1))
- **deps:** require nodejs v18 or later ([e86f5a6](https://gitlab.com/html-validate/html-validate-jest-snapshot/commit/e86f5a69fabdaf73171e868267002bb8dec88128))
- **deps:** support html-validate v9 ([1939347](https://gitlab.com/html-validate/html-validate-jest-snapshot/commit/1939347f355e526ef5cd9352a1f9a4786b3cfe91))
- **deps:** update dependency espree to v10 ([ac4a983](https://gitlab.com/html-validate/html-validate-jest-snapshot/commit/ac4a98325ee931f91f551353ea4f64ea84a16ae1))
- hybrid esm/cjs package ([13533c1](https://gitlab.com/html-validate/html-validate-jest-snapshot/commit/13533c1a3cec6ce52c612bb16a34820d2419b365))

## [5.0.0](https://gitlab.com/html-validate/html-validate-jest-snapshot/compare/v4.0.1...v5.0.0) (2023-06-04)

### ⚠ BREAKING CHANGES

- **deps:** require html-validate v5 or later
- **deps:** require nodejs v16 or later

### Features

- **deps:** require html-validate v5 or later ([0760b3d](https://gitlab.com/html-validate/html-validate-jest-snapshot/commit/0760b3da5c883f3d2195468e9cc0d37a4e52076d))
- **deps:** require nodejs v16 or later ([9913f1e](https://gitlab.com/html-validate/html-validate-jest-snapshot/commit/9913f1ed974edd5918a51998ee8aa0187582bace))

### Dependency upgrades

- **deps:** support html-validate v8 ([7f8226e](https://gitlab.com/html-validate/html-validate-jest-snapshot/commit/7f8226edd5241cf5fa849110a8ca9a2d14b4cb5a))

## [4.0.1](https://gitlab.com/html-validate/html-validate-jest-snapshot/compare/v4.0.0...v4.0.1) (2022-06-17)

### Dependency upgrades

- **deps:** update dependency espree to v9 ([e461a8b](https://gitlab.com/html-validate/html-validate-jest-snapshot/commit/e461a8bcae7b2adbe6939e588191a07e756a7ca0))

## [4.0.0](https://gitlab.com/html-validate/html-validate-jest-snapshot/compare/v3.0.2...v4.0.0) (2022-05-15)

### ⚠ BREAKING CHANGES

- require node 14

### Features

- require node 14 ([dbe7913](https://gitlab.com/html-validate/html-validate-jest-snapshot/commit/dbe7913a9e6752cc3682c6ed69955808f5e1083f))

### Dependency upgrades

- **deps:** support html-validate v7 ([c789fa0](https://gitlab.com/html-validate/html-validate-jest-snapshot/commit/c789fa0064ee1c827afeb24d64034460f21567f4))

### [3.0.2](https://gitlab.com/html-validate/html-validate-jest-snapshot/compare/v3.0.1...v3.0.2) (2021-11-14)

### Dependency upgrades

- **deps:** update dependency html-validate to v6 ([9a1f845](https://gitlab.com/html-validate/html-validate-jest-snapshot/commit/9a1f84582b02f6562de2d230b5e4cbfe76c01192))

### [3.0.1](https://gitlab.com/html-validate/html-validate-jest-snapshot/compare/v3.0.0...v3.0.1) (2021-06-27)

### Dependency upgrades

- **deps:** update dependency html-validate to v5 ([0216e51](https://gitlab.com/html-validate/html-validate-jest-snapshot/commit/0216e5106f9324100ccf9d5841f407c63632ff4c))

## [3.0.0](https://gitlab.com/html-validate/html-validate-jest-snapshot/compare/v2.1.0...v3.0.0) (2021-06-27)

### ⚠ BREAKING CHANGES

- require NodeJS 12

### Features

- require NodeJS 12 ([c3b00a5](https://gitlab.com/html-validate/html-validate-jest-snapshot/commit/c3b00a54acebde88ba35a7a7e08d000fe453f986))

## [2.1.0](https://gitlab.com/html-validate/html-validate-jest-snapshot/compare/v2.0.0...v2.1.0) (2020-11-15)

### Features

- html-validate v4 compatibility ([0dafeec](https://gitlab.com/html-validate/html-validate-jest-snapshot/commit/0dafeecf7ba09ccab207082810d994ef053af1c3))

# [2.0.0](https://gitlab.com/html-validate/html-validate-jest-snapshot/compare/v1.0.0...v2.0.0) (2020-11-01)

### Bug Fixes

- add version check ([b0d4b98](https://gitlab.com/html-validate/html-validate-jest-snapshot/commit/b0d4b983fc9d5879691e0a06c908fe77ba4e9484))
- html-validate@2 compatibility ([ca9cf21](https://gitlab.com/html-validate/html-validate-jest-snapshot/commit/ca9cf21aa9a8697940a6221020c797751126463b))
- **deps:** update dependency acorn-walk to v8 ([a2bca87](https://gitlab.com/html-validate/html-validate-jest-snapshot/commit/a2bca8776537f53674313b586e490419c65e422a))
- **deps:** update dependency espree to v7 ([ccc0ed9](https://gitlab.com/html-validate/html-validate-jest-snapshot/commit/ccc0ed91f92cd570e4d556ddd471b4fc72aab3f7))

### BREAKING CHANGES

- requires html-validate 2 or later

# 1.0.0 (2019-10-09)

### Features

- initial version ([59781cf](https://gitlab.com/html-validate/html-validate-jest-snapshot/commit/59781cf))
